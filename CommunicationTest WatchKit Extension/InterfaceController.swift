//
//  InterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
    
    // MARK: Outlets
    // ---------------------
    @IBOutlet var messageLabel: WKInterfaceLabel!
    // Imageview for the pokemon
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    // Label for Pokemon name (Albert is hungry)
    @IBOutlet var nameLabel: WKInterfaceLabel!
    // Label for other messages (HP:100, Hunger:0)
    @IBOutlet var outputLabel: WKInterfaceLabel!
    
    @IBOutlet var timerLabel: WKInterfaceLabel!
    var pokemonName = ""
    var pokemonType = ""
    var timer = Timer()
    var pokemonSelected = "";
    var hungerLevel = 0
    var healthLevel = 100
    var seconds:Int = 0
    
    var isHibernate = false
    // MARK: Delegate functions
    // ---------------------
    
    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    // 3. Get messages from PHONE
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
        // Message from phone comes in this format: ["course":"MADT"]
        print("Received a message from the phone: \(message)")
        var wakeUpFlag = message["wakeupflag"] as? String
        
        if !message.isEmpty{
            
            if wakeUpFlag == "wakeup" {
                
                    print("Inside wakeup logic")
                
                    self.runTimer()
                
                
                    let receivedPokemonType = message["pokemontype"] as! String
                    let receivedPokemonName = message["pokemonname"] as! String
                    let receivedHealthLevel = message["healthlevel"] as! String
                    let receivedHungerLevel = message["hungerlevel"] as! String
                    
                    if receivedPokemonName != "" {
                        self.nameLabel.setText(receivedPokemonName)
                    }else {
                        self.nameLabel.setText("Choose a name")
                    }
                    
                    if receivedPokemonType != ""  {
                        loadPokemonImage(name: receivedPokemonType)
                    }
                    
                    if receivedHealthLevel != "" && receivedHungerLevel != "" {
                        
                        
                        self.outputLabel.setText("Hunger: \(receivedHungerLevel)  HP: \(receivedHealthLevel)")
                    }
                    
                    messageLabel.setText("Woke Up")
                    wakeUpFlag = "wokeUpAlready"
                }else {
                    let receivedPokemonType = message["pokemontype"] as! String
                    self.messageLabel.setText(receivedPokemonType)
                    self.nameLabel.setText("Choose a name")
                    if pokemonType == ""  {
                        self.pokemonType = receivedPokemonType
                        loadPokemonImage(name: self.pokemonType)
                    }
                    
                }
               
            
        }
        
    }
    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        // 1. Check if teh watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    // MARK: Actions
    // ---------------------
    
    // 2. When person presses button on watch, send a message to the phone
    @IBAction func buttonPressed() {
        
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            self.messageLabel.setText("Sending msg to watch")
            WCSession.default.sendMessage(
                ["name" : "Pritesh"],
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
                    self.messageLabel.setText("Got reply from phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
                self.messageLabel.setText("Error sending message")
            })
        }
        else {
            print("Phone is not reachable")
            self.messageLabel.setText("Cannot reach phone")
        }
    }
    
    
    func loadPokemonImage(name: String) {
        print("Inside load image")
        if name == "pikachu"{
            pokemonImageView.setImage(UIImage.init(named: "pikachu"))
        }
        else if name == "caterpie"{
            pokemonImageView.setImage(UIImage.init(named: "caterpie"))
        }else {
            pokemonImageView.setImage(UIImage.init(named: "pokeball"))
        }
    }
    
    
    // MARK: Functions for Pokemon Parenting
    @IBAction func nameButtonPressed() {
        print("name button pressed")
        let suggestedResponses = ["Pikachu", "Caterpie","Pokeball","Ditto","Raichu"]
        presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) {
            
            (results) in
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                self.pokemonName = userResponse!
                self.nameLabel.setText(userResponse)
                
            }
        }
    }
    
    @IBAction func startButtonPressed() {
        print("Start button pressed")
        self.runTimer()
    }
    
    @IBAction func feedButtonPressed() {
        
        self.hungerLevel  = self.hungerLevel - 12
        if self.hungerLevel < 0 {
            self.hungerLevel = 0
        }
        self.outputLabel.setText("Hunger: \(hungerLevel)  HP: \(healthLevel)")
    }
    
    @IBAction func hibernateButtonPressed() {
        print("Hibernate button pressed")
        timer.invalidate()
        
        if WCSession.default.isReachable {
            print("Attempting to send message to phone")
            self.messageLabel.setText("Status sent")
            let pokemonStatus = ["pokemonname" : self.pokemonName,
                                 "healthlevel" : self.healthLevel,
                                 "hungerlevel" : self.hungerLevel,
                                 "pokemontype" : self.pokemonType] as [String : Any]
            WCSession.default.sendMessage(
                pokemonStatus,
                replyHandler: {
                    (_ replyMessage: [String: Any]) in
                    // @TODO: Put some stuff in here to handle any responses from the PHONE
                    print("Message sent, put something here if u are expecting a reply from the phone")
                    self.messageLabel.setText("Got reply from phone")
            }, errorHandler: { (error) in
                //@TODO: What do if you get an error
                print("Error while sending message: \(error)")
                self.messageLabel.setText("Error sending message")
            })
        }
        else {
            print("Phone is not reachable")
            self.messageLabel.setText("Cannot reach phone")
        }
        
    }
    
    
    func runTimer() {
        print("Inside run timer")
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(InterfaceController.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        self.seconds = self.seconds + 1
        if seconds % 5 == 0 {
            self.hungerLevel = self.hungerLevel + 10
            self.outputLabel.setText("Hunger: \(hungerLevel)  HP: \(healthLevel)")
        }
        
        if self.hungerLevel > 80 {
            self.healthLevel = self.healthLevel - 5
            if self.healthLevel <= 0 {
                self.healthLevel = 0
                self.outputLabel.setText("Hunger: \(hungerLevel)  HP: \(healthLevel)")
                self.nameLabel.setText("\(self.pokemonName) died")
                timer.invalidate()
            }
            self.outputLabel.setText("Hunger: \(hungerLevel)  HP: \(healthLevel)")
        }
        
        self.timerLabel.setText("Time:\(seconds)")
    }
    
    
}
